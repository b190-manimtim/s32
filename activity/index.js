const http = require("http");

// storing the 400 in a variable called port
const port = 4000;

// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {
	/*
		"request" is an object that is sent via the client (browser)
		"url" is the property of the request that refers to the endpoint of the the link
	*/
	// the condition below means that we have to access the localhost:4000 with "/greeting" in its endpoint
	if (request.url === "/" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Welcome to Booking System");
	}

	else if (request.url === "/profile" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Welcome to your profile!");
	}
    else if (request.url === "/courses" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Here’s our courses available");
	}
    else if (request.url === "/addcourse" && request.method === "POST") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Add a course to our resources");
	}
    else if (request.url === "/updatecourse" && request.method === "PUT") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Update a course to our resources");
	}
    else if (request.url === "/archivecourse" && request.method === "DELETE") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Archive courses to our resources");
	}
	/*
		All other routes will return a message of "Page not found"
	*/
	else {
		response.writeHead(404, {"Content-Type": "text/plain"} );
		response.end("Page not found");
	}

});

server.listen(port);

console.log(`Server now running at port: ${port}`);