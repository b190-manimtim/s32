//importing the "http" module
const http = require("http");

// storing the 400 in a variable called port
const port = 4000;

// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {
    // GET REQUEST
    if (request.url === "/items" && request.method === "GET") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Data retrieved from the database");
	};

    // POST REQUEST
    if (request.url === "/items" && request.method === "POST") {
		response.writeHead(200, {"Content-Type": "text/plain"} );
		response.end("Data to be send to the database");
	};
});

// using server and port variables
server.listen(port);

// confirmation that the server is running
console.log(`Server now running at port: ${port}`);

